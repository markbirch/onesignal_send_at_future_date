<?php
/*
Plugin Name:  OneSignal send in future by Mark Birch/Guid Ltd
Plugin URI:   https://bitbucket.org/markbirch/onesignal_send_at_future_date/overview
Description:  Add functionality to send OneSignal push at a future date.
Version:      20180501
Author:       Mark Birch (GUID Ltd) www.gu-id.co.uk
Author URI:   https://www.linkedin.com/in/marbir
License:      GPL2
License URI:  https://www.gnu.org/licenses/gpl-2.0.html
*/


// NOTE you must have the OneSignal plugin installed first, then install this one to add functionality to send at a future date/time.


add_filter('onesignal_send_notification', '_guid_onesignal_send_notification_filter', 10, 4);
function _guid_onesignal_send_notification_filter($fields, $new_status, $old_status, $post) {

    // get the post_onesignal_send_epoch for this post and convert it to date('c')

    $date = $_POST['post_onesignal_send_date'];
    $time = $_POST['post_onesignal_send_time'];

    // make sure there's not any bad data been submitted.
    $sendFullTime = strtotime( sprintf( "%sT%s:00Z", $date, $time ) );
    if( $sendFullTime > time() ) {

        $fields['send_after'] = date('c', $sendFullTime);
    }
    return $fields;
}

// add a date selector to the admin post screen

add_action('admin_notices', '_guid_admin_select_onesignal_time');

/**
 * For Posts, we want to add meta data, so we know what (if any) little icon to add on the bottom right on the tips & previews page.
 * We store in meta data : sport_type_select_name = [soccer|baseball ... etc]
 */
function _guid_admin_select_onesignal_time() {

    global $post;
    if( !$post ) return;
    add_meta_box( 'post_onesignal_send_time', 'OneSignal Send Time', "_guid_render_post_onesignal_send_time", 'post', 'side', 'high' );
}

/**
 * Print the HTML needed to write the select element for onesignal send time
 * @param WP_Post $post    The current post.
 * @param array $metabox With metabox id, title, callback, and args elements.
 * @param $currentSelection
 */
function _guid_render_post_onesignal_send_time( $post, $metabox ) {

    $currentSendEpochDate = date('Y-m-d');
    $currentSendEpochTime = date('H:i');

    $ret = 'Send at: <input type="date" size="10" name="post_onesignal_send_date" value="' . $currentSendEpochDate . '" /> <input size="5" type="time" name="post_onesignal_send_time" value="' . $currentSendEpochTime . '" /> GMT';

    // we have to do this - *so* sorry!!
    print $ret;
}


