
Add functionality to the OneSignal Wordpress plugin, so that you can select to send the push notification at a future date.

NOTE that this plugin is not written by the authors of onesignal, but relies on that plugin being installed. It adds a new select box on the admin screen where you can select the date & time to send the notification.


TO INSTALL
==========

- copy this whole directory into your wp-content/plugins directory
- go to [your site]/wp-admin & log in
- select 'Plugins' from the main menu
- find 'Onesignal send in future' and click 'activate'
- add a new post or edit an existing one, and see an extra box on the right, to select when to sent the OneSignal message.

NOTE that 'OneSignal Push Notifications' MUST be activated for this plugin to work.